FROM debian:bullseye-slim

RUN apt-get -y update && \
	DEBIAN_FRONTEND=noninteractive \
	apt-get install -y gunicorn python3-django python3-pgmagick python3-pil python3-magic && \
	DEBIAN_FRONTEND=noninteractive \
	apt-get -y upgrade && \
	rm -rf /var/lib/apt/lists/*

ENV PYTHONUNBUFFERED 1

EXPOSE 8000

WORKDIR /srv

COPY . ./

RUN mkdir -p storage
RUN mkdir -p originals
RUN mkdir -p small
RUN mkdir -p thumbnails

RUN groupadd -r user && useradd -d /srv -r -g user user
RUN chown -R user:user /srv
USER user

RUN ls -l
RUN chmod +x startup.sh
ENTRYPOINT ["/srv/startup.sh"]