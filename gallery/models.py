from django.db import models
from django.conf import settings

from PIL import Image

import os
import hashlib


TYPE_CHOICES = [
    (0, 'Folder'),
    (1, 'Image'),
    (2, 'Video'),
]


class Object(models.Model):
    type = models.SmallIntegerField(choices=TYPE_CHOICES)
    hash = models.CharField(max_length=64)
    path = models.CharField(max_length=2048)
    parent = models.CharField(max_length=64)
    size = models.CharField(max_length=64)
    thumb = models.CharField(max_length=65536)

    @classmethod
    def create(cls, path, parent, type, size_str, thumb):
        print(hashlib.sha256(path.encode()).hexdigest())
        return cls(path=path, parent=parent, hash=hashlib.sha256(path.encode()).hexdigest(), type=type, size=size_str, thumb=thumb)



