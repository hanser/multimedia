from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from . import views


urlpatterns = [
    path('start', views.start, name='start'),
    path('gallery/<str:media_object>', views.index, name='index'),
    path('indexing', views.indexing, name='indexing'),
    path('gallery/media/<str:media_object>', views.media, name='media'),
    path('gallery/original/<str:media_object>', views.original, name='original'),
] + \
              staticfiles_urlpatterns()

