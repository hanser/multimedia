import os
from django.conf import settings
from django.shortcuts import render
from django.http import HttpResponseRedirect

from .models import Object
import hashlib
from PIL import Image, ImageOps
import magic
import io
import base64
from django.http import HttpResponse
from wsgiref.util import FileWrapper


smallSize = 1280, 1280
thumbnailSize = 50, 50


def start(request):
    obj = Object.objects.filter(parent="")
    return HttpResponseRedirect('/gallery/%s' % obj[0].hash)


def index(request, media_object):
    print(media_object)
    context = {'folders': list(Object.objects.filter(parent=media_object, type=0).order_by("path")),
               'files': list(Object.objects.filter(parent=media_object, type=1).order_by("path"))}
    return render(request, "gallery/index.html", context)


def media(request, media_object):
    print("media "+media_object)
    filename = "small/"+Object.objects.filter(hash=media_object, type=1)[0].path
    #print(filename)
    data = None
    with open(filename, 'rb') as f:
        data = f.read()
    response = HttpResponse(data, content_type='image/jpeg')
    #response['Content-Disposition'] = 'attachment; filename=myfile.zip'
    return response
    #context = {'folders': list(Object.objects.filter(parent=media_object, type=0).order_by("path")),
    #           'files': list(Object.objects.filter(parent=media_object, type=1).order_by("path"))}
    #return render(request, "gallery/index.html", context)


def original(request, media_object):
    print("original "+media_object)
    filename = "originals/"+Object.objects.filter(hash=media_object, type=1)[0].path
    #print(filename)
    data = None
    with open(filename, 'rb') as f:
        data = f.read()
    response = HttpResponse(data, content_type='image/jpeg')
    response['Content-Disposition'] = 'attachment; filename='+os.path.basename(filename)
    return response


def indexing(request):
    for root, subFolders, files in os.walk(settings.ORIGINALS_ROOT):
        root = os.path.relpath(root, settings.ORIGINALS_ROOT)
        if root == '.':
            parent_folder_hash = ""
            if not Object.objects.filter(parent=''):
                print("added root", root)
                new_object = Object().create('', '', 0, "0", "")
                new_object.save()
            root = ''
        parent_hash = hashlib.sha256(root.encode()).hexdigest()
        for folder in subFolders:
            folder_name = os.path.join(root, folder)
            folder_hash = hashlib.sha256(folder_name.encode()).hexdigest()
            if not Object.objects.filter(hash=folder_hash):
                try:
                    os.mkdir(os.path.join(settings.SMALL_ROOT, folder_name))
                    os.mkdir(os.path.join(settings.THUMBS_ROOT, folder_name))
                    print("added new folder", folder_name, folder_hash)
                    new_object = Object().create(folder_name, parent_hash, 0, "0", "")
                    new_object.save()
                except FileExistsError:
                    print("folder already existed", folder_hash)
        for file in files:
            file_name = os.path.join(root, file)
            file_hash = hashlib.sha256(file_name.encode()).hexdigest()
            if not Object.objects.filter(hash=file_hash):
                print("added new file", file_name)
                mime = magic.from_file(os.path.join(settings.ORIGINALS_ROOT, file_name), mime=True)

                if mime == 'image/jpeg':
                    im = Image.open(os.path.join(settings.ORIGINALS_ROOT, file_name))
                    im.thumbnail(smallSize, Image.ANTIALIAS)
                    im.save(os.path.join(settings.SMALL_ROOT, file_name), "JPEG")
                    thumbstr = ""
                    thumb = ImageOps.fit(im, thumbnailSize, Image.ANTIALIAS)
                    with io.BytesIO() as thumboutput:
                        thumb.save(thumboutput, format="JPEG")
                        thumbstr = base64.b64encode(thumboutput.getvalue()).decode('utf-8')
                        print(thumbstr)
                    #thumb.save(os.path.join(settings.THUMBS_ROOT, file_name), "JPEG")
                    width, height = im.size
                    size_str = str(width) + "x" + str(height)
                    Object().create(file_name, parent_hash, 1, size_str, thumbstr).save()
                elif mime == 'video/mp4':
                    Object().create(file_name, parent_hash, 2, "0", "").save()
                else:
                    print(mime)

    return HttpResponseRedirect('start')
