#!/bin/sh

python3 manage.py migrate

ls -l

id

chown user:user storage

exec gunicorn -b :8000 --access-logfile - --error-logfile - --log-level INFO multimedia.wsgi
